PHP Session Fixation CheatSheet
===============================

Below, we will concider that the session ID cookie is called `PHPSESSID`.

* In GET
  ```http
  GET /script.php?PHPSESSID=my_session_name HTTP/X.X
  Host: example.com
  ```
  Requires:
    - `session.use_only_cookies=0` in `php.ini` (not default!)
    - No HTTP referer in another website (Check [`ext/session/session.c`, line 1580](https://github.com/php/php-src/blob/2d48d734a20192a10792669baaa88dbe86f2b3a6/ext/session/session.c#L1580))
  Source: [`ext/session/session.c`, line 1550](https://github.com/php/php-src/blob/2d48d734a20192a10792669baaa88dbe86f2b3a6/ext/session/session.c#L1550)
* In POST
  ```http
  POST /script.php HTTP/X.X
  Host: example.com
  
  PHPSESSID=my_session_name
  ```
  Requires:
    - `session.use_only_cookies=0` in `php.ini` (not default!)
    - No HTTP referer in another website (Check [`ext/session/session.c`, line 1580](https://github.com/php/php-src/blob/2d48d734a20192a10792669baaa88dbe86f2b3a6/ext/session/session.c#L1580))
  Source: [`ext/session/session.c`, line 1557](https://github.com/php/php-src/blob/2d48d734a20192a10792669baaa88dbe86f2b3a6/ext/session/session.c#L1557)
* In REQUEST
  ```http
  XXX /PHPSESSID=my_session_name/script.php HTTP/X.X
  Host: example.com
  ```
  Requires:
    - `session.use_only_cookies=0` in `php.ini` (not default!)
    - No HTTP referer in another website (Check [`ext/session/session.c`, line 1580](https://github.com/php/php-src/blob/2d48d734a20192a10792669baaa88dbe86f2b3a6/ext/session/session.c#L1580))
  Source: [`ext/session/session.c`, line 1566](https://github.com/php/php-src/blob/2d48d734a20192a10792669baaa88dbe86f2b3a6/ext/session/session.c#L1566)

