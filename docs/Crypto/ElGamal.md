ElGamal
=======

## Génération de clé

Soit le nombre premier $p$, le groupe cyclique $\mathbb{Z}^*_p$

Soit $g \in \mathbb{Z}^*_p$ d'ordre $q|(p-1)$

$sk = x$

$pk = g^x \pmod p$

---

**Public key**                          | **Private key**
----------------------------------------|-------------------------
$p$ et $g$, paramètres publiques        | $sk = x$, exposant privé
$pk = g^x \pmod p$, clé de vérification |


## ElGamal Encryption

**Key Generation** | **Encryption**                     | **Decryption**
-------------------|------------------------------------|--------------------
$pk = g^x \pmod p$ | $E(pk=g^x, m) = (C, D)$            | $D(sk=x, (C, D)) = m$
$sk = x$           | $(C, D) = (g^r, (g^x)^rm) \pmod p$ | $m = (D/C^x) \pmod p$
                   |                                    | $\Leftrightarrow m = \frac{(g^x)^rm}{(g^r)^x} \pmod p$


## ElGamal Signature

**Key Generation** | **Signature**                         | **Verification**
-------------------|---------------------------------------|--------------------------------------------------------------------------------------
$pk = g^x$         | $S(sk=x, m) = \sigma \pmod q$         | $V(pk=g^r, m, \sigma) = yes/no$
$sk = x$           | $\sigma = (C, D) = (g^r, (m-xg^r)/r)$ | $g^m \stackrel{?}{=} C^D(g^x)^C \pmod p$
                   |                                       | $\Leftrightarrow g^m \stackrel{?}{=} (g^r)^{\frac{m-xg^r}{r}}(g^x)^{g^r} \pmod p$
                   |                                       | $\Leftrightarrow g^m \stackrel{?}{=} g^{\frac{r \times (m-xg^r)}{r}+ xg^r} \pmod p$
