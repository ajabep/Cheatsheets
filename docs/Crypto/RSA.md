RSA
===

## Key generation

$n = p * q$, $p$ and $q$ prime numbers

$e$, coprime with $\varphi(n) = (p-1)(q-1)$

$d$, as $d \cdot e = 1 \pmod{\varphi(n)}$, so $d \cdot e + u \varphi(n) = 1$

---

**Public key**              | **Private key**
----------------------------|---------------------------------
$n = pq$, public modulus  | $d = e^{-1} \pmod{\varphi(n)}$
$e$, the encryption key   |


## RSA Encryption

**Key Generation** | **Encryption**         | **Decryption**
-------------------|------------------------|--------------------
$pk = e$         | $E(pk=(e,n), m) = C$ | $D(sk=d, C) = m$
$sk = d$         | $C = m^e \pmod n$    | $m = C^d \pmod n$


## RSA Signature

**Key Generation** | **Signature**            | **Verification**
-------------------|--------------------------|---------------------------------------
$pk = e$         | $S(sk=d, m) = \sigma$  | $V(pk=(e,n), m, \sigma) = yes/no$
$sk = d$         | $\sigma = m^d \pmod n$ | $m \stackrel{?}{=} \sigma^e \pmod n$


## Attacks

* Factorisation of module $n$
	- Trial Division ($p$ or $q$ too small)
	- Fermat Factorisation
	- Pollard's Rho Factorisation ($p$ or $q$ too small)
	- Pollard's p-1 Factorisation ($p-1$ or $q-1$ too smooth ; in to stage if $p-1$ or $q-1$ is nearly too smooth)
	- Williams' p+1 Factorisation
	- ECM
	- EECM-MPFQ
	- HECM
	- GEECM (quantic)
