DSA
===

## Génération de clé

Soit les longueurs $L$ et $N$, $L$ divisible par 64.

Soit $p$ un nombre premier de longueur $L$

Soit $q$ un nombre premier de longueur $N$, tel que $p-1 = qz$ avec $z$ un entier

Soit $h$ tel que $1 < h < p-1$ de sorte que $g = h^z > 1 \pmod p$

Soit $x$ aléatoire tel que $0 < x < q$

$y = g^x = (h^z)^x \pmod p$


$sk = x$

$pk = (p, q, g=h^z, y=g^x)$

---

**Public key**              | **Private key**
----------------------------|---------------------------
$pk = (p, q, g=h^z, y=g^x)$ | $sk = x$


## DSA Signature

**Key Generation**          | **Signature**                              | **Verification**
----------------------------|--------------------------------------------|--------------------------------------------------------------------------------------
$pk = (p, q, g=h^z, y=g^x)$ | $S(sk=x, m) = \sigma$                      | $V(pk=(p, q, g=h^z, y=g^x), m, \sigma) = yes/no$
$sk = x$                    | $\sigma = (s_1, s_2)$                      | $s_1 \stackrel{?}{=} g^{u1} \cdot y^{u2} \pmod p \pmod q$
|                           | Avec $s_1 = g^r \pmod p \pmod q$           | Avec $u1 = H(m) \times s_2^{-1} \pmod q$
|                           | Avec $s_2 = \frac{H(m) + s_1x}{r} \pmod q$ | Avec $u2 = s_1 \times s_2^{-1} \pmod q$
|                           | Avec $r$ aléatoire                         | $\Leftrightarrow g^r \stackrel{?}{=} g^{H(m) \times s_2^{-1}} \cdot (g^x)^{s_1 \times s_2^{-1}} \pmod p \pmod q$
                            |                                            | $\Leftrightarrow g^r \stackrel{?}{=} g^{\frac{H(m)r}{H(m) + xg^r}} \cdot g^{\frac{xrg^r}{H(m) + xg^r}} \pmod p \pmod q$
                            |                                            | $\Leftrightarrow g^r \stackrel{?}{=} g^{\frac{r\times(H(m) + xg^r)}{H(m) + xg^r}} \pmod p \pmod q$


## Attacks

* Discrete logarithm
	- BSGS
	- CRT (composite and square-free order)
	- Pohlig-Hellman (composite order)
* Bad random ($r$ not random)
