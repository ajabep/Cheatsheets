# Exporting a secret with a double Base64

In order to avoid detection during attack-defense CTF.

The fact to try to identify a flag leaving a system is not a good way to identify if you have been breached. Indeed an attacker can be stealth using encoding (e.g.: enconding in UTF-16, encoding in base64). However, a motivated defenser can be as crazy as trying to check these kind of stuff. To avoid that, here is a cheatsheet to dump secrets using a double Base64 encoding.


## SQL

Example to encode the `password` column from the `users` table.

### MySQL

```sql
-- Since MySQL 5.6
SELECT TO_BASE64(TO_BASE64(password)) FROM users;
```

### MSSQL

```sql
-- Tested on MS SQL Server 2017, should works since MS SQL Server 2005.
SELECT CAST(N'' AS XML).value(
  'xs:base64Binary(sql:column("b64"))',
  'NVARCHAR(MAX)'
)
FROM (
  SELECT CAST(
    CAST(N'' AS XML).value(
      'xs:base64Binary(sql:column("bin"))',
      'NVARCHAR(MAX)'
    )
    AS VARBINARY(MAX)
  ) as b64
  FROM (
    SELECT CAST(password AS VARBINARY(MAX)) AS bin
    FROM users
  ) as tmp
) as tmp
```


### SQLite

```sql
-- From a vanilla installation, we cannot encode in base64. But we can hex-encode a string/binary.
-- Tested in SQLite 3.26, but in the sources since, at least, 2008.
SELECT hex(hex(password)) FROM users
```


### PostGreSQL

```sql
-- Tested since PostGreSQL 9.4
select encode(
  encode(
    password::bytea,
    'base64'
  )::bytea,
  'base64'
) from users;
```


### Google BigQuery
### DB2
### HQL
### Oracle SQL



## Programming languages

Example to encode the `password` variable.

### NodeJS / JavaScript

```js
// Works from IE10+ or since 2011
var encoded = btoa(btoa(password));
```


### PHP

```php
// Works since, at least, PHP4
echo base64(base64($password));
```


### Python

```python
# Works from, at least, Python 2.7
import base64
encoded = base64.b64encode(base64.b64encode(password))
```


## Shells

Dump the file `flag` in a double Base64 encoding.

### Bash / Linux

```bash
# TODO to test
# Each line is a different way to do so
openssl enc -base64 <flag | openssl enc -base64
python -m base64 <flag | python -m base64
base64 -w0 <flag | base64 -w0
perl -MMIME::Base64 -ne 'printf "%s\n",encode_base64($_)' <flag | perl -MMIME::Base64 -ne 'printf "%s\n",encode_base64($_)'
ruby -rbase64 -e 'puts Base64.encode64(STDIN.read)' <flag | ruby -rbase64 -e 'puts Base64.encode64(STDIN.read)'
emacs -Q --batch  -eval '(princ (base64-encode-string (read-string ": ")))' <flag | emacs -Q --batch  -eval '(princ (base64-encode-string (read-string ": ")))'
```


### /bin/sh

```sh
# TODO to test
```


### Powershell

```pwsh
[convert]::ToBase64String(([System.Text.Encoding]::Unicode.GetBytes(([convert]::ToBase64String((Get-Content -path ".\flag" -Encoding byte))))))
```


### CMD / Windows

```cmd
"C:\Program Files\Git\usr\bin\base64" -w 0  && REM "TODO"

REM "Requires %TEMP%\cert does not exists" && certutil -encode ./flag %TEMP%\cert && findstr /v /c:- %TEMP%\cert > %TEMP%\b64 && del %TEMP%\cert && certutil -encode %TEMP%\b64 %TEMP%\cert && findstr /v /c:- %TEMP%\cert > %TEMP%\b64 && notepad %TEMP%\b64
```


## Misc

### GraphQL
### LDAP
### NoSQL stuff?
### XXE


## Template engines

### TWIG

```twig
{# TODO #}
```


### Smarty

```smarty
{# TODO #}
```


### Jinja

```jinja
{# TODO #}
```


### Django

```django
{# TODO #}
```


## Vulns

Example of exploit string to leak something using the double encoding.

### PHP filters

Dump the `flag` file.

```php
TODO
```

