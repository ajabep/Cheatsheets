# Dump databases using direct connections

## SQLMap

Use the `--direct` option:

```sh
# Direct connection over the network (MariaDB, etc.)
sqlmap --direct DBMS://USER:PASSWORD@DBMS_IP:DBMS_PORT/DATABASE_NAME --dump-all

# Direct connection over a file (SQLite, MS Access, FireBird etc.)
sqlmap --direct DBMS://DATABASE_FILEPATH --dump-all
```

If the DBMS requires a NTLM authentication, you will need the Python module `python-ntlm`. Check out documentation here: https://github.com/mullender/python-ntlm.


## MS Access

See "SQLMap", using `schema` = `access`.

You will need the Python module `pyodbc` to be able to use it.


## IBM DB2

See "SQLMap", using `schema` = `db2`.

You will need the Python module `ibm-db` to be able to use it.


## Firebird

See "SQLMap", using `schema` = `firebird`.

You will need the Python module `KInterbasDB` to be able to use it. Check out documentation here: https://kinterbasdb.sourceforge.net/dist_docs/.


## MS SQL Server

See "SQLMap", using `schema` = `mssqlserver`.

You will need the Python module `pymssql` >= 1.0.2 to be able to use it.



## Mysql / Percona / MariaDB

See "SQLMap", using `schema` = `mysql`.

You will need the Python module `PyMySQL` to be able to use it. However, the remote DBMS have to be one of the following:

- MySQL >= 5.7
- MariaDB >= 10.3


## Oracle

See "SQLMap", using `schema` = `oracle`.

You will need the Python module `cx-Oracle` to be able to use it.


## PostGreSQL

See "SQLMap", using `schema` = `postgresql`.

You will need the Python module `psycopg2` to be able to use it.


## SQLite

See "SQLMap", using `schema` = `sqlite`.

For SQLite 2, you will need the Python module [`python-pysqlite2`](https://code.google.com/archive/p/pysqlite/), but it's a broken link.


## Sybase

See "SQLMap", using `schema` = `sybase`.

You will need the Python module `pymssql` >= 1.0.2 to be able to use it.


## HSQLDB

See "SQLMap", using `schema` = `hsqldb`.

You will need the Python module `JayDeBeApi` and `JPype1` to be able to use it. I don't already tested it, but, it should also requires a Java Runtime (JRE).


## Informix

See "SQLMap", using `schema` = `informix`.

You will need the Python module `ibm-db` to be able to use it.


## MonetDB

See "SQLMap", using `schema` = `monetdb`.

You will need the Python module `pymonetdb` to be able to use it.


## Apache Derby

See "SQLMap", using `schema` = `derby`.

You will need the Python module `pydrda` to be able to use it.


## Vertica

See "SQLMap", using `schema` = `vertica`.

You will need the Python module `vertica_python` to be able to use it.


## Presto

See "SQLMap", using `schema` = `presto`.

You will need the Python module `presto-python-client` to be able to use it.


## MimerSQL

See "SQLMap", using `schema` = `mimersql`.

You will need the Python module `mimerpy` to be able to use it.


## ClickHouse

See "SQLMap", using `schema` = `clickhouse`.

You will need the Python module `clickhouse-connect` to be able to use it.


## CrateDB

See "SQLMap", using `schema` = `cratedb`.

You will need the Python module `psycopg2` to be able to use it.


## Cubrid

See "SQLMap", using `schema` = `cubrid`.

You will need the Python module [`CUBRIDdb`](https://github.com/CUBRID/cubrid-python) to be able to use it.


## InterSystems Cache

See "SQLMap", using `schema` = `cache`.

You will need the Python module `JayDeBeApi` and `JPype1` to be able to use it. I don't already tested it, but, it should also requires a Java Runtime (JRE).


## Redis

```sh
redis-cli -u redis://USER:PASSWORD@REDIS_IP:REDIS_PORT/DB_NUM --rdb /tmp/dump.rdb
```

This can be provided using the `redis:alpine` docker image:

```sh
docker run -it --rm redis:alpine -v ${PWD}:/myvolume redis-cli ARGS
```


## MangoDB

```sh
mongodump mongodb://USERNAME:PASSWORD@IP_MONGO:PORT_MONGO/
```

This tools in embeded in the docker image `mango:latest`.

The dumped data is put into the `dump/` directory. To convert the BSON files into JSON ones, use the `bsondump` utils, also embeded in this docker image.


## MemCached

Unfortunately, MemCached works over a raw TCP socket and does not provide any tool.

To dump/backup memcache, you can use :

- (all a library with a lot of tools) memcdump from [libmemcached.org](https://libmemcached.org)
- (minimalist python script) My small snippet: https://gitlab.com/-/snippets/2519993
